//let arrayOfObjects = require("./data")

 //    Implement a loop to access and log the city and country of each individual in the dataset.
      
 function cityAndCountryOfEachIndividual(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        let eachIndividual = []
        for(let elements of arrayOfObjects){
            eachIndividual[elements.name]={}
            eachIndividual[elements.name]["city"]=elements.city
            eachIndividual[elements.name]["country"]=elements.country
        };
        return eachIndividual
    }else{
        return null
    }
 };
 
 //console .log(cityAndCountryOfEachIndividual(arrayOfObjects))
 
 module.exports = cityAndCountryOfEachIndividual 
