
//let arrayOfObjects = require("./data.js")

//    Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.
   
    function students(arrayOfObjects,country){
        if(Array.isArray(arrayOfObjects)){
            let newArray=[]
            for(let elements of arrayOfObjects){
                if(elements.isStudent && elements.country === country){
                    newArray.push(elements.name)
                }

            }
            return newArray
        }else{
            return null
        }
    }
    //console.log(students(arrayOfObjects,"Australia"))

    module.exports = students
    