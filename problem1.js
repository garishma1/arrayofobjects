//let arrayOfObjects = require("./data.js")

//    Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.
function emailAddresses(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        let newArray=[]
        for(let elements of arrayOfObjects){
            newArray.push(elements.email)
        }
         return newArray
    }else{
        return null
    }

}

module.exports = emailAddresses