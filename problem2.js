
//let arrayOfObjects = require("./data.js")

//    Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.
   

   function hobbiesOf30YearsOld(arrayOfObjects,age){
    if(Array.isArray(arrayOfObjects)){
        let newArray=[]
        for(let elements of arrayOfObjects){
            if(elements.age === age){
                newArray.push(elements.hobbies)

            }
        }
        return newArray
    }else{
        return null
    }
   }
   //console.log(hobbiesOf30YearsOld(arrayOfObjects))
   module.exports = hobbiesOf30YearsOld;
