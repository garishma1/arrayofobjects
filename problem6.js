
//let arrayOfObjects = require("./data.js")

//    Create a function to retrieve and display the first hobby of each individual in the dataset.


     function firsthobbyOfEachIndividual(arrayOfObjects){
        if(Array.isArray(arrayOfObjects)){
            let newArray =[]
            for(let elements of arrayOfObjects){
                newArray.push(elements.hobbies[0])
            }
            return newArray 
         
        }else{
            return null
        }
    }
     
     //console .log(firsthobbyOfEachIndividual(arrayOfObjects))

     module.exports = firsthobbyOfEachIndividual
     
         