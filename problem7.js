
//let arrayOfObjects = require("./data.js")

//   Write a function that accesses and prints the names and email addresses of individuals aged 25.
         
function namesAndEmailAddressesAged25(arrayOfObjects){
    if(Array.isArray(arrayOfObjects)){
        let newArray=[]
        for(let elements of arrayOfObjects){
            if(elements.age == 25){
                newArray.push(elements.name )
                newArray.push(elements.email)
            }
        }
        return newArray 
    }else{
        return null
    }
    
}
 //console.log(namesAndEmailAddressesAged25(arrayOfObjects))

 module.exports = namesAndEmailAddressesAged25
 