//let arrayOfObjects = require("./data.js")

//    Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.
         
    function namesAndCityOfIndexPosition3(arrayOfObjects,position){
        if(Array.isArray(arrayOfObjects)){
            let individualDetails={}
            for(let index=0;index<arrayOfObjects.length;index++){
                if(index===position){
                    individualDetails["name"]=arrayOfObjects[index].name;
                    individualDetails["city"]=arrayOfObjects[index].city;
                }
                
            }
            return individualDetails;
        }else{
            return null
        }
    }
    //console.log(namesAndCityOfIndexPosition3(arrayOfObjects,3))

    module.exports = namesAndCityOfIndexPosition3

    